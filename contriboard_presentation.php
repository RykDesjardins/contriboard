<?php
	include_once('contriboard_db.php');

	function cb_admin_present() {
	  $cbdb = new Contriboard_DB();
	  $payscales = $cbdb->getScales();
	  $users = $cbdb->getUsers();
	  $stats = $cbdb->generateTableRows();
	  $singles = $cbdb->getSingles();
	  $period = $cbdb->getPeriod();

	  $singleIterator = 0;
	  $singleIteratorMax = 0;
?>

<script>
  var payScales = [
    {"minimum":-1,"amount":0}
    <?php foreach ($payscales as $payscale) : ?>
    ,{"minimum":<?=$payscale->scale?>,"amount":<?=$payscale->amount?>}
    <?php endforeach; ?>
  ];
  
  var fbcache = [];
  
  var bigurl = "https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&ids=http://www.mtlblog.com<?php foreach($stats as $stat) : if ($stat['uniqid'] != '') : ?>,<?=$stat['urls']?><?php endif; endforeach; ?>";
  
  var scrapeEverything = function($) {
	$.get(bigurl, function(data) {
		for (var key in data){
			if (typeof data[key].is_scraped != 'undefined') {
				$.post('https://graph.facebook.com/?id='+key+'&scrape=true');
			}
		}
		
		getShared($);
	});
  };
  
  var getShared = function($) {
    <?php foreach($stats as $stat) : if ($stat['uniqid'] != '') : ?>
    $.ajax({
      url: "https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&ids=<?=$stat['urls']?>",
      method: "GET",
      from: 'localhost',
      proxy: true,
      async: true,
      success: function(fb) {
	var sharetotal = 0;
	var moneytotal = 0;
	
	for (var key in fb){
	  var curshare = typeof fb[key].shares == 'undefined' ? 0 : fb[key].shares;
	  
	  if (typeof fb[key].is_scraped != 'undefined' && curshare == 0) {
	    $.ajax({
	      url : 'https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&id='+key+'&scrape=true',
	      type : 'POST',
	      async : false
	    });
	    
	    $.ajax({
	      url : 'https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&id=' + key,
	      type : 'GET',
	      async : false,
	      success : function(data) {
	        curshare = typeof data.shares == 'undefined' ? 0 : data.shares;
	      }
	    });
	  } else if (curshare == 0) {
	    $.ajax({
	      url : 'https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&id=' + key.substring(0, key.length - 1),
	      type : 'GET',
	      async : false,
	      success : function(data) {
	        curshare = typeof data.shares == 'undefined' ? 0 : data.shares;
	      }
	    });		
	  }
	  
	  var money = 0;
	  
	  for (var i = 0; i < payScales.length; i++) {
	    if (curshare > payScales[i].minimum) {
	      money = payScales[i].amount;
	    } else {
	      break;
	    }
	  }
	  
	  moneytotal += money;
	  sharetotal += curshare;   
	  fbcache[key] = curshare;
	}
	
	$("#<?php echo $stat['uniqid']; ?> td.shares").html(sharetotal);
	$("#<?php echo $stat['uniqid']; ?> td.money").html(moneytotal + "$");
      }
    });
    <?php endif; endforeach; ?>
  };
  
  var computeRemainingTime = function() {
    jQuery('.daysremaining').each(function(index, td) {
      var postdate = jQuery(td).data("date");
      
      var date1 = new Date(postdate);
      var date2 = new Date();
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
      
      jQuery(td).html((15-diffDays) + " day" + (15-diffDays==1?"":"s")); 
    });	
  };
  
  var toggleAuthorDetails = function(id, full) {
    id = full ? id : "#" + id + "-details";
    
    if (!full) {
      if (jQuery(id).hasClass('hidden')) {
        jQuery(id).removeClass('hidden');
      } else {
        jQuery(id).addClass('hidden');
      }
    }
    
    if (full || !jQuery(id).hasClass('fbcached')) {
      var shareLimit = payScales[payScales.length - 1].minimum;
      jQuery(id).find('.hot-icon').remove();
    
      jQuery(id).find('.single-table .single-shares').each(function(index, single) {
	var fbkey = jQuery(single).data('fbkey');
	var count = fbcache[fbkey];
	var money = 0;
	
	jQuery(single).html(typeof count == 'undefined' ? "0" : fbcache[fbkey]);		
	
	if (typeof count != 'undefined') {
	  for (var i = 0; i < payScales.length; i++) {
	    if (count > payScales[i].minimum) {
	      money = payScales[i].amount;
	    } else {
	      break;
	    }
	  }	
	
	  var percent = (count/shareLimit)*100;
	  percent = percent > 100 ? 100 : percent;
	  
	  var r = 255;
	  var g = Math.ceil(255-(180*(percent/100)));
	  var b = Math.ceil(255-(255*(percent/100)));
	  
	  var color = "rgb(" + r + "," + g + "," + b + ")";
	  
	  jQuery(single).parent().find('.single-money-progress').css({
		'width' : percent + "%",
		'background-color' : color
	  });
	  jQuery(single).parent().find('.single-money-span').html(money + "$");
	  
	  if (percent == 100) {
	    jQuery(single).parent().find('.single-money-bar').css({
	      'border' : '1px solid #E33',
	      'box-shadow' : '0px 0px 5px rgba(255, 70, 0, 0.6)'
	    })
	      .after('<img class="hot-icon" src="<?=plugins_url() . "/contriboard/hot.png"?>" />');
	  }
	}
      });
      
      jQuery(id).addClass('fbcached');
    }
  }
  
  var refreshEverything = function() {
    getShared(jQuery);
    computeRemainingTime();
    
    setTimeout(function() {
      jQuery('.fbcached').each(function(index, tr) {
	toggleAuthorDetails('#' + jQuery(tr).removeClass('fbchached').attr('id'), true);
      });
    }, 5000);
  };
  
  jQuery(document).ready(function($) {
    getShared($);
    computeRemainingTime();
    jQuery.get("<?=plugins_url() . "/contriboard/hot.png"?>");
    
    // setInterval(refreshEverything, 15000);
  });
</script>

<script>
  <?php $isMtlBlog = (!is_multisite() && strpos($_SERVER["HTTP_HOST"],'mtlblog') !== false); ?>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?=$isMtlBlog?"UA-29129013-4":"UA-54434926-5"?>', 'auto');
  ga('send', 'pageview');
</script>

<div class="wrap">
  <h2>Contributor board</h2>
  <h3 class="title">Report from <b><?=$period['start']?></b> to <b><?=$period['end']?></b></h3>
  
  <p>You can request a previously generated report underneath this table</p>
  <div class="contrib-table-wrapper">
    <table class="wp-list-table widefat">
      <thead>
	<tr>
	  <th>Contributor</th>
	  <th>Articles Written</th>
	  <th>Number of shares</th>
	  <th>Money earned</th>
	</tr>
      </thead>
      <tbody>
      <?php foreach($stats as $stat) : if ($stat['uniqid'] != '') : ?>
	<tr id="<?=$stat['uniqid']?>">
	  <td><a href="javascript:toggleAuthorDetails('<?=$stat['uniqid']?>', false);"><?=$stat['user_name']?></a></td>
	  <td><?=$stat['article_count']?></td>
	  <td class="shares">loading...</td>
	  <td class="money">loading...</td>
	</tr>
	<tr id="<?=$stat['uniqid']?>-details" class="hidden">
	  <td colspan="5">
	    <table class="wp-list-table widefat single-table">
	      <thead>
	        <tr>
		  <th>Title</th>
		  <th>Number of shares</th>
		  <th>Money earned</th>
		  <th>Days Remaining</th>
		</tr>
	      </thead>
	      <tbody>
	      <?php $singleIteratorMax += $stat['article_count']; ?>
	      <?php for($singleIterator; $singleIterator < $singleIteratorMax; $singleIterator+=1) : ?>
		<tr>
		  <td class="single-title"><a href="<?=$singles[$singleIterator]['url']?>" target="_blank"><?=$singles[$singleIterator]['post_title']?></a></td>
		  <td class="single-shares" data-fbkey="<?=$singles[$singleIterator]['url']?>">loading...</td>
		  <td class="single-money">
			<div class="single-money-bar">
				<div class="single-money-progress"></div>
			</div>
			<span class="single-money-span"></span>
		  </td>
		  <td class="daysremaining" data-date="<?=$singles[$singleIterator]['post_date']?>"></td>
		</tr>
	      <?php endfor; ?>
	      </tbody>
	    </table>
	  </td>
	</tr>
      <?php endif; endforeach; ?>
      </tbody>
    </table>
  </div>	

  <h3 class="title">Get report archive</h3>
  <p>From the
    <select class="archive-round-select" id="archive-round-select">
      <option value="1">1st to the 14th</option>
      <option value="2">15th to the last day</option>
    </select>
    of
    <select class="archive-month-select" id="archive-month-select">
    <?php
      for ($m=1; $m<=12; $m++) {
        $month = date('F', mktime(0,0,0,$m, 1, date('Y')));
        echo '<option value="'.$m.'">' .$month. '</option>';
      }
    ?>
    </select>
    <select class="archive-year-select" id="archive-year-select">
      <option value="2015">2015</option>
    </select>
  </p>
  <button class="button" id="buttonGenerateArchive" data-bid="<?=get_current_blog_id()?>">Generate</button>

  <div class="contrib-archive-wrapper hidden">
    <table class="wp-list-table widefat">
      <thead>
	<tr>
	  <th>Contributor</th>
	  <th>Article title</th>
	  <th>Archived date</th>
	  <th>Number of shares</th>
	  <th>Money earned</th>
	</tr>
      </thead>
      <tbody>
	<tr>
	  <td></td>
	  <td></td>
	  <td></td>
	  <td class="shares"></td>
	  <td class="money"></td>
	</tr>
      </tbody>    
    </table>
  </div>
  <p class="contrib-archive-message hidden">No data found.</p>
</div>

<?php 	
} 

function cb_settings_present() {   
  $blogname = '';
	
  if ( is_multisite() ) {
    $cursite = get_current_site();
    $blogname = $cursite->site_name . " " . get_bloginfo('name');
  } else {
    $blogname = get_bloginfo('name');
  }
  
  $cbdb = new Contriboard_DB();
  $rows = $cbdb->getScales();
?>

<div class="wrap">
  <h2>Contributor board settings</h2>
  <h3 class="title">For <b><?=$blogname?></b></h3>

  <p>A scale is the minimum share count required to earn the amount of money specified. A scale of 0 means that posting an article is worth an amount of money even if not shared. Scales are reordered ascending automatically. If you need to add a scale between two existing one, simply insert it at the bottom and it will be automatically placed between the desired values. </p>
  <p>If you want to <b>remove</b> a scale, simply leave the box empty.</p>

  <div class="contrib-config-table-wrapper">
    <table>
      <thead>
	<tr>
	  <th>Scale</th>
	  <th>Amount of $</th>
	</tr>
      </thead>
      <tbody>
      <?php foreach($rows as $row) : ?>
	<tr>
	  <td><input class="tbScale" type="text" value="<?=$row->scale?>" /></td>
	  <td><input class="tbAmount" type="text" value="<?=$row->amount?>" /></td>
	</tr>
      <?php endforeach; ?>
      </tbody>
    </table>
    <button class="button" id="addScaleButton">Add scale</button>
    <button class="button" id="saveScales" data-bid="<?=get_current_blog_id()?>">Save</button>
  </div>	
</div>
<?php 
}

function cb_balance_present() {
	$cbdb = new Contriboard_DB();
	$rows = array();
	$ctrname = "";
	$totalmoney = 0;
	
	if (current_user_can('manage_options')) {
		$rows = $cbdb->getAllBalances(get_current_blog_id());
		$ctrname = "all contributors";
	} else {
		$rows = $cbdb->getBalance(get_current_user_id(), get_current_blog_id());
        
        $usrdata = get_userdata(get_current_user_id());
        $ctrname = $usrdata->first_name . ' ' . $usrdata->last_name;
	}

?>
<script>
  <?php $isMtlBlog = (!is_multisite() && strpos($_SERVER["HTTP_HOST"],'mtlblog') !== false); ?>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?=$isMtlBlog?"UA-29129013-4":"UA-54434926-5"?>', 'auto');
  ga('send', 'pageview');
</script>
<script>
	var clearBalance = function(userid) {
		var asker = new RykAsker(jQuery);
		asker.ask({
			type : "ask",	
			title : "Clear contributor's balance",
			questions : [
				{
					id : "hbank",
					text: "Input bank confirmation number",
					required : true,
					type: "text"
				}
			],
			buttons : [
				{
					text : "OK",
					type : "accept",
					validate : true,
					action : function(data, dlg) {
						var bank = data.answers.hbank.text;
						jQuery('#user' + userid).find('.balancerow').html('Loading...');

						jQuery.ajax({
							url : '/wp-admin/admin-ajax.php',
							data: {'action':'cb_clear_balance','uid':userid,'bank':bank,'bid':<?=get_current_blog_id()?>},
							type: 'POST',
							success: function(data) {
								jQuery('#user' + userid).data('balance', '0').find('.balancerow').html('0$');
							}
						});											
					}
				},
				{
					text : "Cancel",
					type : "decline",
				}				
			]
		});
	};

	var payBalance = function(userid) {
		var asker = new RykAsker(jQuery);
		asker.ask({	
			type : "ask",	
			title : "Pay balance",
			autodismiss : true,
			questions : [
				{
					id : "hpay",
					text: "Input desired amount",
					required : true,
					type: "text"
				},
				{
					id : "hbank",
					text: "Input bank confirmation number",
					required : true,
					type: "text"
				}
			],
			buttons : [
				{
					text : "Pay",
					type : "accept",
					validate : true,
					action : function(data) {
						var amount = parseInt(data.answers.hpay.text);
						var bank = data.answers.hbank.text;

						if (amount && !isNaN(amount)) {
							jQuery('#user' + userid).find('.balancerow').html('Loading...');
							var curVal = parseInt(jQuery('#user' + userid).data('balance'));
							amount = parseInt(amount);

							jQuery.ajax({
								url : '/wp-admin/admin-ajax.php',
								data: {'action':'cb_pay_balance','uid':userid,'amt':amount,'bank':bank,'bid':<?=get_current_blog_id()?>},
								type: 'POST',
								success: function(data) {
									jQuery('#user' + userid).data('balance', curVal-amount).find('.balancerow').html((curVal-amount) + '$');
								}
							});
						}
					}
				},
				{
					text : "Cancel",
					type : "decline"
				}
			]
		});	
	};

	var modifyBalance = function(userid) {
		var asker = new RykAsker(jQuery);
		asker.ask({	
			type : "ask",	
			title : "Modify balance",
			autodismiss : true,
			questions : [
				{
					id : "hpay",
					text: 'Input desired amount (prepend "-" for substractions)',
					required : true,
					type: "text"
				}
			],
			buttons : [
				{
					text : "Pay",
					type : "accept",
					validate : true,
					action : function(data) {
						var amount = parseInt(data.answers.hpay.text);

						if (amount && !isNaN(amount)) {
							jQuery('#user' + userid).find('.balancerow').html('Loading...');
							var curVal = parseInt(jQuery('#user' + userid).data('balance'));
							amount = parseInt(amount);

							jQuery.ajax({
								url : '/wp-admin/admin-ajax.php',
								data: {'action':'cb_adjust_balance','uid':userid,'amt':amount,'bid':<?=get_current_blog_id()?>},
								type: 'POST',
								success: function(data) {
									jQuery('#user' + userid).data('balance', curVal+amount).find('.balancerow').html((curVal+amount) + '$');
								}
							});
						}
					}
				},
				{
					text : "Cancel",
					type : "decline"
				}
			]
		});	
	};
</script> 
<div class="wrap">
  <h2>Balances <?=current_user_can('manage_options') ? "(admin)" : ""?></h2>
  <h3 class="title">For <b><?=$ctrname?></b></h3>

  <div class="contrib-balance-wrapper">
    <table class="wp-list-table widefat">
      <thead>
		<tr>
	      <th>Contributor</th>
	      <th>Current balance</th>
		  <?php if (current_user_can('manage_options')) : ?>
		  <th>Actions</th>
		  <?php endif; ?>
		</tr>
      </thead>
      <tbody>
		<?php foreach ($rows as $row) : ?>
		<?php 
			$current_user = new WP_User($row['userid']);
			$roles = $current_user->roles;	

			if ($roles[0] == 'staff_contributor'/* or $roles[0] == 'subscriber'*/) :
				$totalmoney += $row["balance"];
		?>
		<tr id="user<?=$row['userid']?>" data-balance="<?=$row['balance']?>">
	  	  <td><?=$row["username"]?></td>
	  	  <td class="balancerow"><?=$row["balance"]?>$</td>
		  <?php if (current_user_can('manage_options')) : ?>
		  <td>
			<a href="javascript:clearBalance(<?=$row['userid']?>);">Clear balance</a> | 
			<a href="javascript:payBalance(<?=$row['userid']?>);">Pay amount</a> | 
			<a href="javascript:modifyBalance(<?=$row['userid']?>);">Adjust balance</a>
		  </td>
		  <?php endif; ?>
		</tr>
		<?php endif; endforeach; ?>
		<?php if (current_user_can('manage_options')) : ?>
		<tr>
		  <td><b>Total</b></td>
		  <td><b><?=$totalmoney?>$</b></td>
		  <td></td>
		</tr>
		<?php endif; ?>
      </tbody>    
    </table>
  </div>

  <?php if (current_user_can('manage_options')) : ?>
  <?php
	$news = $cbdb->getUsersActionFeed();
  ?>
  <h3>Recent actions</h3>
  <div class="contrib-newsfeed-wrapper">
    <table class="wp-list-table widefat">
      <thead>
		<tr>
	      <th>Action</th>
	      <th>Details</th>
		  <th>Date</th>
		</tr>
      </thead>
      <tbody>
		<?php foreach ($news as $row) : ?>
		<tr>
	  	  <td><?=$row["message"]?></td>
	  	  <td><?=$row["details"]?></td>
		  <td><?=$row["date"]?></td>
		</tr>
		<?php endforeach; ?>
      </tbody>    
    </table>
  </div>
  <?php endif; ?>
</div>
<?php } 

function cb_present_user_balance() {
	$cbdb = new Contriboard_DB();

	$payscales = $cbdb->getScales();
	$users = $cbdb->getUsers();
	$stats = $cbdb->generateTableRows();

	$row = $cbdb->getBalance(get_current_user_id(), get_current_blog_id())[0];
	$news = $cbdb->getUserActionFeed(get_current_user_id(), 'balance', get_current_blog_id());
	$nextPushDate = $cbdb->getNextPushDate();
	$archivetotal = $cbdb->getUserArchiveTotal();

    $usrdata = get_userdata(get_current_user_id());
	$ctrname = $usrdata->first_name . ' ' . $usrdata->last_name;
?>

<script>
  var payScales = [
    {"minimum":-1,"amount":0}
    <?php foreach ($payscales as $payscale) : ?>
    ,{"minimum":<?=$payscale->scale?>,"amount":<?=$payscale->amount?>}
    <?php endforeach; ?>
  ];
  
  var fbcache = [];
  
  var bigurl = "https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&ids=http://www.mtlblog.com<?php foreach($stats as $stat) : if ($stat['uniqid'] != '') : ?>,<?=$stat['urls']?><?php endif; endforeach; ?>";
  
  var scrapeEverything = function($) {
	$.get(bigurl, function(data) {
		for (var key in data){
			if (typeof data[key].is_scraped != 'undefined') {
				$.post('https://graph.facebook.com/?id='+key+'&scrape=true');
			}
		}
		
		getShared($);
	});
  };
  
  var getShared = function($) {
    <?php foreach($stats as $stat) : if ($stat['uniqid'] != '') : ?>
    $.ajax({
      url: "https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&ids=<?=$stat['urls']?>",
      method: "GET",
      from: 'localhost',
      proxy: true,
      async: true,
      success: function(fb) {
	var sharetotal = 0;
	var moneytotal = 0;
	
	for (var key in fb){
	  var curshare = typeof fb[key].shares == 'undefined' ? 0 : fb[key].shares;
	  
	  if (typeof fb[key].is_scraped != 'undefined' && curshare == 0) {
	    $.ajax({
	      url : 'https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&id='+key+'&scrape=true',
	      type : 'POST',
	      async : false
	    });
	    
	    $.ajax({
	      url : 'https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&id=' + key,
	      type : 'GET',
	      async : false,
	      success : function(data) {
	        curshare = typeof data.shares == 'undefined' ? 0 : data.shares;
	      }
	    });
	  } else if (curshare == 0) {
	    $.ajax({
	      url : 'https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&id=' + key.substring(0, key.length - 1),
	      type : 'GET',
	      async : false,
	      success : function(data) {
	        curshare = typeof data.shares == 'undefined' ? 0 : data.shares;
	      }
	    });		
	  }
	  
	  var money = 0;
	  
	  for (var i = 0; i < payScales.length; i++) {
	    if (curshare > payScales[i].minimum) {
	      money = payScales[i].amount;
	    } else {
	      break;
	    }
	  }
	  
	  moneytotal += money;
	  sharetotal += curshare;   
	  fbcache[key] = curshare;
	}
	
		$(".last-14-days-live").html(moneytotal + ".00$");
      }
    });
    <?php endif; endforeach; ?>
  };

  jQuery(document).ready(function($) {
    getShared($);
  });
</script>

<script>
  <?php $isMtlBlog = (!is_multisite() && strpos($_SERVER["HTTP_HOST"],'mtlblog') !== false); ?>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', '<?=$isMtlBlog?"UA-29129013-4":"UA-54434926-5"?>', 'auto');
  ga('send', 'pageview');
</script>

<div class="wrap">
  <h2>Payments</h2>
  <div class="balance-cards">
	<div class="half-col-rect full-height">
		<div class="float-square full-height">
		  <div class="float-stats full-height">
			<h2>Last 14 days (live)</h2>
			<p>Worth of all your articles that were posted from 14 days ago to today. Articles that were posted exactly 14 days ago will be transfered to "Pending Approval" around midnight.</p>
			<p class="big-stats-num shown last-14-days-live">loading...</p>
		  </div>
		</div>
	</div>
	
	<div class="half-col-rect full-height">
		<div class="float-square half-height">
		  <div class="float-stats full-height">
			<h2>Pending approval</h2>
			<p>Will be reviewed by the staff and pushed to your balance on <b><?=$nextPushDate?></b></p>
			<p class="big-stats-num shown"><?=$archivetotal?>.00$</p>
		  </div>
		</div>
		<div class="float-square half-height">
		  <div class="float-stats full-height">
			<h2>Current Balance</h2>
			<p>Balance payments are processed twice a month.</p>
			<p class="big-stats-num shown"><?=$row["balance"]?>.00$</p>
		  </div>
		</div>
	</div>

  </div>

  <h3 class="title">Latest events</h3>
  <div class="contrib-balance-wrapper">
    <table class="wp-list-table widefat">
      <thead>
		<tr>
	      <th>Event</th>
	      <th>Date</th>
		</tr>
      </thead>
      <tbody>
		<?php foreach ($news as $new) : ?>
		<tr>
	  	  <td><?=$new["message"]?></td>
	  	  <td><?=$new["date"]?></td>
		</tr>
		<?php endforeach; ?>
      </tbody>    
    </table>
  </div>
</div>

<?php } ?>
