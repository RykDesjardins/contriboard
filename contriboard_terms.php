<?php

function cb_terms_check() {
	global $user_level;
	
	if ($user_level != -1 and ($_GET['page'] == 'contriboard' or $_GET['page'] == 'contriboardbalance')) {
		// Verify meta data
		$current_user = wp_get_current_user();
		$checked = $current_user->get('contriboard_has_agreed');

		if (!isset($checked) or $checked != 1) {
			cb_print_terms_script();
			add_action('admin_head', 'cb_print_terms_dialog');
		}
	}	
}

function cb_accept_terms() {
	$user_id = get_current_user_id();
	update_user_meta( $user_id, 'contriboard_has_agreed', 1 );
	
	echo uniqid() . uniqid();
	exit();
}

add_action('wp_ajax_cb_accept_terms', 'cb_accept_terms');
function cb_print_terms_script() {
	?>
		<script>
			// alert('You have to agree');
			var acceptTerms = function() {
				var $ = jQuery;
			
				$('.terms-actions .message').show();
				$('.terms-actions button').hide();
			
				$.ajax({
					url:"<?=get_site_url()?>/wp-admin/admin-ajax.php",
					type:'POST',
					data:'action=cb_accept_terms',
					success: acceptCallback
				});
			}
			
			var acceptCallback = function(data) {
				var $ = jQuery;
				
				$('#bigTerms').fadeOut(500);
				$('.terms-actions .message').hide();
			}
		</script>
		<style>
			.terms-dim {
				position:fixed;
				top:0;bottom:0;left:0;right:0;
				
				background-color: rgba(0, 0, 0, 0.3);
				z-index:100000;
			}
			
			.terms-wrapper {
				position: absolute;
				top:20px;
				bottom:20px;
				left:20px;
				right:20px;
				
				padding:20px;
				background-color: rgba(250, 250, 250, 0.95);
				
				box-shadow: 0px 0px 20px 3px rgba(0, 0, 0, 0.5);
				
				-webkit-border-radius: 2px;
				   -moz-border-radius: 2px;
				    -ms-border-radius: 2px;
				     -o-border-radius: 2px;
					border-radius: 2px;
				
			}
			
			.terms-container {
				position:absolute;
				
				top:20px;
				bottom:70px;
				left:20px;
				right:20px;
				
				border: 1px solid #999;
				overflow-y:scroll;
				
				background-color: #FFF;
			}
			
			.terms-container .terms-text {
				width:auto;
				height:100%;
				
				resize: none;
				
				color: #333;
				font-family: Arial, Helvetica, sans-serif;
				
				padding: 5px;
			}
			
			.terms-actions {
				width:auto;
				position:absolute;
				
				bottom:20px;
				height:30px;
				
				left:20px;
				right:20px;
				
				text-align:right;
			}
			
			.terms-actions button {
				border: 1px solid #96c56f;
				height: 100%;
				
				color: #32772B;
				font-weight: bold;
				
				padding-left:10px;
				padding-right: 10px;
				
				cursor: pointer;
				
				background: #a9db80;
				background: -moz-linear-gradient(top, #a9db80 0%, #96c56f 100%);
				background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a9db80), color-stop(100%,#96c56f));
				background: -webkit-linear-gradient(top, #a9db80 0%,#96c56f 100%);
				background: -o-linear-gradient(top, #a9db80 0%,#96c56f 100%);
				background: -ms-linear-gradient(top, #a9db80 0%,#96c56f 100%);
				background: linear-gradient(to bottom, #a9db80 0%,#96c56f 100%);
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a9db80', endColorstr='#96c56f',GradientType=0 );
				
				-webkit-border-radius: 2px;
				   -moz-border-radius: 2px;
				    -ms-border-radius: 2px;
				     -o-border-radius: 2px;
					border-radius: 2px;				
			}
			
			.terms-actions .message {
				display: none;
			}
		</style>
	<?php
}

function cb_print_terms_dialog() {
	?>
		<div class="terms-dim" id="bigTerms">
			<div class="terms-wrapper">
				<div class="terms-container">
					<div class="terms-text">
<?php
	$s = (strpos(get_site_url(),'mtlblog') !== false) ? "MTL Blog" : "Narcity";
?>
<p lang="en-GB" align="center"><span style="font-size: medium;"><span style="color: #00000a;"><span style="font-family: Arial,serif;"><span style="font-size: large;">MTL Blog Inc.</span></span></span></span></p>
<p lang="en-GB" align="center"><span style="font-size: medium;"><span style="color: #00000a;"><span style="font-family: Arial,serif;"><span style="font-size: large;">Terms and Conditions of Payment</span></span></span></span></p>

<p>
Subject to the terms and conditions of the Contributor Agreement, you will receive payment related to the amount of valid impressions of ads displayed on your contributor piece, valid clicks on ads displayed on your contributor piece, or other valid reach in connection with your contributor piece, in each case as determined by MTL Blog Inc., and set forth in the Contributor Terms and Conditions. MTL Blog Inc., will pay you as a contributor, by direct deposit within one month of publication of your contributing article or blog post.
</p>
<p>
To ensure timely payment, you are responsible for submitting your direct deposit information directly to our Accounting department via email: accounting@mtlblog.com  
</p>
<p>
You are responsible for keeping the Accounting department up to date on any contact information changes, including address, phone and email address changes, as well as any direct deposit information changes. You are responsible for any and all banking charges by your bank institution in relation to direct deposit payments.
</p>
<p>
Payments will be solely calculated based on <?=$s?>’s accounting. 
</p>
<p>
Payments to you may be withheld or or adjusted in the case of invalid activity, which is determined by MTL Blog Inc. Such activity includes, and is not limited to spam, invalid impressions, invalid clicks on ads or links generated by an automatic program, bot, or similar; false representation; or clicks and impressions generated from your IP address.
</p>
	<p>
Additionally, MTL Blog Inc. may withhold payment in order to offset any fees owed to MTL Blog Inc., by you.
</p>
<p>
All payments to you from MTL Blog Inc. are to be accounted for as tax inclusive. Any tax burden on amounts paid to you from MTL Blog Inc., are your responsibility.
</p>
					</div>
				</div>
				
				<div class="terms-actions">
					<button onclick="acceptTerms(); return false;">Accept and continue</button>
					<span class="message">Please wait...</class>
				</div>
			</div>
		</div>
	<?php
}

?>
