<?php

class Contriboard_DB {
  private $results = '';
  private $hasEntry = false;
  private $period = '';
  private $rows = array();
  private $stats = array();
  
  public function __construct() {
    
  }
  
  public function getUsers($datestart = '', $dateend = '', $posttype = 'post', $forceadmin = false) {
    global $wpdb;
    $now = getdate();
    $exactDate = $dateend === 'same';
    
    if (!$exactDate) {
      $twoWeeksAgo = strtotime( $datestart == '' ? date('Y-m-j') . " -2 weeks" : $datestart );   
      $datestart = $datestart == '' ? date('Y-m-d', $twoWeeksAgo) : $datestart;
      
      $this->period = array(
	"start" => $datestart,
	"end"   => ($dateend == '' ? 'today ('.date('Y-m-j').')' : $dateend)
      );      
    }
    
    $request = "
      SELECT user.id as user_id, user.display_name as 'user_name', DATE(post.post_date) AS post_date, post.post_title, post.id as 'post_id', post.post_type, post.post_name as slug
      FROM `".$wpdb->prefix."posts` post 
      INNER JOIN ".$wpdb->base_prefix."users user
      ON user.id = post.post_author 
      WHERE 
      ".($exactDate ? "DATE(post_date) = '" . $datestart . "' " : 
      "DATE(post_date) > '".$datestart."'
      AND DATE(post_date) < ".($dateend == '' ? 'NOW()' : ("'" . $dateend . "'")))." 
      AND post_type = '".$posttype."' 
      AND post_status = 'publish' 
      ".($forceadmin || current_user_can('manage_options') ? "" : ("AND user.id = " . get_current_user_id()))." 
      ORDER BY user.id
    ";
	
    $this->results = $wpdb->get_results($request);
    return $this->getIterator();
  }
  
  public function getIterator() {
    return $this->results;
  }
  
  public function getPeriod() {
    return $this->period;
  }
  
  public function generateTableRows($results = '') {
    $results = $results === '' ? 
      ($this->results === '' ? 
	$this->getUsers() :
	$this->results) : 
      $results;
      
    $this->results = $results;
    
    foreach ($results as $row) {
      array_push($this->rows, array(
	'user_name'	=> $row->user_name,
	'user_id'	=> $row->user_id,
	'post_title'	=> $row->post_title,
	'post_id'	=> $row->post_id,
	'post_type'	=> $row->post_type,
	'slug'		=> $row->slug,
	'post_date'	=> $row->post_date,
	'url'		=> get_the_permalink($row->post_id)
      ));
    }
    
    $currentuser = '';
    $currentindex = -1;
    $this->stats = array();
    
    foreach ($this->rows as $row) {
      if ($currentuser != $row['user_name']) {
	$currentuser = $row['user_name'];
	$currentindex += 1;
	
	$this->stats[] = array(
	  "article_count"	=> 0,
	  "urls"		=> "",
	  "uniqid"		=> uniqid(),
	  "user_name"		=> $currentuser,
	  "user_id"		=> $row['user_id']
	);
      }
      
      $this->stats[$currentindex]['article_count'] += 1;
      $this->stats[$currentindex]['urls'] .= ($this->stats[$currentindex]['article_count'] == 1 ? "" : ",") . $row['url'];
    }
    
    return $this->stats;
  }
  
  public function getNativeRows() {
    return $this->rows;
  }
  
  public function getSingles() {
    return $this->rows;
  }
  
  public function getScales() {
    global $wpdb;
    $request = "
      SELECT * FROM ".$wpdb->base_prefix."contriboard_payscale WHERE 
      blog_id = '" . get_current_blog_id() . "' ORDER BY scale";
      
    $this->results = $wpdb->get_results($request);
    return $this->getIterator();   
  }

  public function getArchive($round, $month, $year, $blogid, $fromconsole = false) {
    global $wpdb;
    $datestart = $year . "-" . $month . "-" . ($round == 1 ? "1" : "15");
    $dateend   = $year . "-" . $month . "-" . ($round == 1 ? "14" : cal_days_in_month(CAL_GREGORIAN, $month, $year));
    
    $request = "
      SELECT * FROM ".$wpdb->base_prefix."contriboard_earnings WHERE 
      archived_date >= '".$datestart."' AND archived_date <= '".$dateend."' AND blog_id = ".$blogid.($fromconsole || current_user_can('manage_options') ? "" : " AND user_id = " . get_current_user_id())." ORDER BY user_id, archived_date";
    
    $this->results = $wpdb->get_results($request);
    return $this->getIterator();    
  }

  public function getUserArchiveTotal() {
	$currentmonth = intval(date('n'));
	$currentday   = intval(date('j'));
	$currentyear  = intval(date('Y'));
	$currentround = $currentday > 14 ? 2 : 1;

	$rows = $this->getArchive($currentround, $currentmonth, $currentyear, get_current_blog_id());
	$total = 0;	

	foreach ($rows as $row) {
	  $total += $row->moneyearned;
	}

	return $total;
  }

  public function getNextPushDate() {
	$currentday   = intval(date('j'));
	$currentmonth = intval(date('n'));
	$currentyear  = intval(date('Y'));

	$endday = $currentday;
	$endmonth = $currentmonth;
	$endyear = $currentyear;

	if ($currentday > 14) {
		$endday = 1;	

		if ($currentmonth == 12) {
			$endyear += 1;
			$endmonth = 1;
		} else {
			$endmonth += 1;
		}
	} else {
		$endday = 15;
	}	

	$strdate = ($endday < 10 ? "0".$endday : $endday) . "-" . ($endmonth<10?"0".$endmonth:$endmonth) . "-" . $endyear;

	return DateTime::createFromFormat('d-m-Y', $strdate)->format('F jS, Y');;
  }

  public function getBalance($userid, $blogid) {
	global $wpdb;
	$balance = 0;	

	$req = "SELECT balance FROM " . $wpdb->base_prefix . "contriboard_balance WHERE user_id = " . 
		$userid . " AND blog_id = " . 
		$blogid;

	$diffs = $wpdb->get_results($req);
	foreach ($diffs as $bal) {
		$balance += $bal->balance;
	}

	return array(array(
		"balance"  => $balance,
		"userid"   => $userid,
		"username" => get_userdata($userid)->display_name
	));
  }

  public function getAllBalances($blogid) {
	global $wpdb;
	$balances = array();
	$blogid = isset($blogid) ? get_current_blog_id() : $blogid;

	$req = "SELECT balance, user_id FROM " . $wpdb->base_prefix . "contriboard_balance WHERE blog_id = " . 
		$blogid . " ORDER BY user_id";
	
	$diffs = $wpdb->get_results($req);
	$cuserid = -1;
	$index = -1;
	foreach ($diffs as $bal) {
		if ($bal->user_id != $cuserid) {
			$index += 1;
			$cuserid = $bal->user_id;		

			$balances[$index] = array(
				"balance"  => 0,
				"userid"   => $cuserid,
				"username" => get_userdata($cuserid)->display_name
			);
		}

		$balances[$index]["balance"] += $bal->balance;
	}

	return $balances;
  }

  public function getUserActionFeed($userid, $type, $blogid) {
	global $wpdb;
	$news = array();
	$blogid = isset($blogid) ? get_current_blog_id() : $blogid;
	
	$req = "SELECT message, date_pushed FROM ".$wpdb->base_prefix."contriboard_user_event
		WHERE user_id = ".$userid." AND type = '".$type."' AND blog_id = ".$blogid."
	";
	$results = $wpdb->get_results($req);
	foreach ($results as $res) {
	  $news[] = array(
		"message" => $res->message,
		"date" => $res->date_pushed
	  );
	}

	return $news;
  }

  public function getUsersActionFeed($type, $blogid) {
	global $wpdb;
	$news = array();
	$blogid = !isset($blogid) ? get_current_blog_id() : $blogid;
	$type = !isset($type) ? 'balance' : $type;
	
	$req = "SELECT message, details, date_pushed FROM ".$wpdb->base_prefix."contriboard_user_event
		WHERE type = '".$type."' AND blog_id = ".$blogid." ORDER BY date_pushed DESC LIMIT 100	
	";

	$results = $wpdb->get_results($req);
	foreach ($results as $res) {
	  $news[] = array(
		"message" => $res->message,
		"details" => $res->details,
		"date" => $res->date_pushed
	  );
	}

	return $news;
  }

  public function clearBalance($userid, $bank, $blogid) {
	global $wpdb;
	global $current_user;
	get_currentuserinfo();
	$displayname = get_the_author_meta('display_name', $userid);
	
	$req = "DELETE FROM " . $wpdb->base_prefix . "contriboard_balance WHERE user_id = " . 
		$userid . " AND blog_id = " . 
		$blogid;

	$wpdb->query($req);

	$req = "INSERT INTO ".$wpdb->base_prefix."contriboard_user_event VALUES(".
		$userid.", ".
		$blogid.", 'balance', ".
		"'A payment was sent to ".$displayname." and the entire balance is now empty.', '".$bank."', NOW()".
	");";		

	$wpdb->query($req);
	return 1;
  }

  public function payBalance($userid, $sub, $bank, $blogid) {
	global $wpdb;
	$displayname = get_the_author_meta('display_name', $userid);

	$req = "INSERT INTO " . $wpdb->base_prefix . "contriboard_balance VALUES(" .
		$userid . ", " . 
		$blogid . ", -" . 
		$sub . 
	");";

	$wpdb->query($req);
	
	$req = "INSERT INTO ".$wpdb->base_prefix."contriboard_user_event VALUES(".
		$userid.", ".
		$blogid.", 'balance', ".
		"'A payment of ".$sub."$ was sent to ".$displayname.", and that amount was removed from current balance.', '".$bank."', NOW()".
	");";		

	$wpdb->query($req);
	return 1;
  }

  public function adjustBalance($userid, $mod, $blogid) {
	global $wpdb;
	global $current_user;
	get_currentuserinfo();
	$displayname = get_the_author_meta('display_name', $userid);

	$req = "INSERT INTO " . $wpdb->base_prefix . "contriboard_balance VALUES(" .
		$userid . ", " . 
		$blogid. ", " . 
		$mod . 
	");";

	$wpdb->query($req);

	$req = "INSERT INTO ".$wpdb->base_prefix."contriboard_user_event VALUES(".
		$userid.", ".
		$blogid.", 'balance', ".
		"'".$displayname."''s balance was adjusted by ".$mod."$', 'Executed by ".$current_user->display_name."', NOW()".
	");";		

	$wpdb->query($req);
	return 1;
  }
  
  public function hasEntry() {
    
  }
  
  public function getEntry() {
    
  }
}

?>
