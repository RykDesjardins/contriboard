<?php
/*
Plugin Name:	Contriboard
Plugin URI:	
Description:	Contriboard is a leaderboard used by admins to manage the contributors money scale
Version:	0.1 Beta
Author:		Érik Desjardins
Author URI:	erikdesjardins.com
License:	GPLv2
*/	
?><?php

include_once('contriboard_presentation.php');
include_once('contriboard_terms.php');

add_action('admin_menu', 'cb_admin_menu');
add_action('admin_enqueue_scripts', 'cb_print_scripts');
add_action('wp_ajax_saveCBScales', 'cb_save_scales');
add_action('wp_ajax_getCBArchive', 'cb_get_archive');
add_action('cb_read_contrib_payscale', 'cb_store_contrib_payscale' );

register_activation_hook(__FILE__, 'cb_activation');

function cb_admin_menu() {
	global $isMtlBlog;

	$current_user = wp_get_current_user();
	$roles = $current_user->roles;	
	
	if ($roles[0] == 'administrator') {
		add_menu_page( 
			'Contriboard', 
			'Contriboard', 
			'manage_options', 
			'contriboard', 
			'cb_admin_present', 
			'dashicons-feedback', 
			'5.112' 
		);

		add_submenu_page( 
			'contriboard', 
			'Balances', 
			'Balances', 
			'manage_options', 
			'contriboardbalance', 
			'cb_balance_present'
		);
	} elseif ($roles[0] == 'staff_contributor') {
		add_menu_page( 
			'Contriboard', 
			'Contriboard', 
			'read', 
			'contriboard', 
			'cb_admin_present', 
			'dashicons-feedback', 
			'5.112' 
		);

		add_submenu_page( 
			'contriboard', 
			'Payments', 
			'Payments', 
			'read', 
			'contriboardbalance', 
			'cb_present_user_balance'
		);	
	}
	
	add_submenu_page(
		'options-general.php',
		'Contriboard Settings',
		'Contriboard',
		'edit_users',
		'contriboard-settings',
		'cb_settings_present'
	);

	$scr = get_current_screen();

	$isMtlBlog = (!is_multisite() && strpos($_SERVER["HTTP_HOST"],'mtlblog') !== false);
}

function cb_print_scripts() {
	wp_register_script( 
	    'contriboardconfigjs', 
	    plugins_url() . '/contriboard/contriboard.js', 
	    array( 'jquery' )
	);
	wp_enqueue_script( 'contriboardconfigjs' );	
	
	echo '<link rel="stylesheet" type="text/css" href="'.plugins_url().'/contriboard/contriboard.css?0.9.4" />';
	echo "<link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>";

	cb_terms_check();
}

function cb_activation() {
	global $wpdb;

	if ( is_multisite() ) {
		if ( !empty( $_GET['networkwide'] ) ) {
			$start_blog = $wpdb->blogid;
			$blog_list = $wpdb->get_col( 'SELECT blog_id FROM ' . $wpdb->blogs );

			foreach ( $blog_list as $blog ) {
				switch_to_blog( $blog );
				qquiz_create_table( $wpdb->get_blog_prefix() );
			}

			switch_to_blog( $start_blog );
			return;
		}
	}

	cb_create_table($wpdb->base_prefix);
	cb_hook_cron();
}

function cb_create_table($prefix) {
	global $wpdb;

	$creation_query = 
		'CREATE TABLE IF NOT EXISTS ' .$prefix. 'contriboard_earnings (
			`user_id` INT NOT NULL,
			`article_id` BIGINT,
			`sharetotal` MEDIUMINT,
			`moneyearned` SMALLINT,
			`archived_date` DATE,
			`blog_id` VARCHAR(100)
		);';

	$wpdb->query($creation_query);

	
	$creation_query = 
		'CREATE TABLE IF NOT EXISTS ' .$prefix. 'contriboard_payscale (
			`site_name` VARCHAR(100) NOT NULL,
			`blog_id` VARCHAR(100) NOT NULL,
			`scale` SMALLINT,
			`amount` SMALLINT		
		);';

	$wpdb->query($creation_query);

	$creation_query = 
		"CREATE TABLE IF NOT EXISTS ".$prefix."contriboard_user_event (
			user_id INT,
			blog_id VARCHAR(100), 
			type VARCHAR(10),
			message VARCHAR(200),
			date_pushed DATETIME
	);";

	$wpdb->query($creation_query);

	$creation_query = 
		"CREATE TABLE IF NOT EXISTS ".$prefix."contriboard_balance (
			user_id INT,
			blog_id VARCHAR(100),
			balance FLOAT
	);";

	$wpdb->query($creation_query);
}

function cb_hook_cron() {
	wp_clear_scheduled_hook( 'cb_read_contrib_payscale' );
	$timestamp = wp_next_scheduled( 'cb_read_contrib_payscale' );

	if ( $timestamp == false) {
		wp_schedule_event( time() - 5*60*60, 'daily', 'cb_read_contrib_payscale' );
	}
}

function cb_start_contrib_payscale() {	
	display_errors();
}

function cb_store_contrib_payscale() {

	global $wpdb;
	set_time_limit(0);
	ignore_user_abort(1);
	
	$twoweeksago = new DateTime();
	
	$cbdb = new Contriboard_DB();
	$cbdb->generateTableRows($cbdb->getUsers($twoweeksago->modify('-2 weeks')->format('Y-m-d'), 'same'));
	$rows =	$cbdb->getNativeRows();
	$scalesdb = $cbdb->getScales();
	$scales = array();
	$req = '';

	foreach ($scalesdb as $scale) {
		$scales[] = array('minimum' => $scale->scale, 'amount' => $scale->amount);
	}

	$url = 'https://graph.facebook.com/?access_token=369886419698055|Tarzhg4y9bqvp24TWRJ3y1ujAVE&ids=';	

	foreach ($rows as $row) {
		$ch = curl_init($url . $row['url']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, 60000);
		$data = json_decode(curl_exec($ch));
		curl_close($ch);

		$currentuserid = $row['user_id'];

		foreach ($data as $key => $value) {
			$shares = isset($value->shares) ? $value->shares : 0;
			$money = 0;

			foreach ($scales as $scale) {
				if ($shares >= $scale['minimum']) {
					$money = $scale['amount'];
				} else {
					break;
				}
			}

			$req = "INSERT INTO " . $wpdb->base_prefix . "contriboard_earnings VALUES(".$currentuserid.", ".$row['post_id'].", ".$shares.", ".$money.", NOW(), ".get_current_blog_id().");";
			$wpdb->query($req);
		}
	}
}
add_action('wp_ajax_forceScaleCalc', 'cb_store_contrib_payscale'); 

function cb_save_scales() {
	global $wpdb;
	$payscales = $_POST["payscales"];
	$blogid = $_POST["bid"];
	$req = "DELETE FROM `".$wpdb->base_prefix."contriboard_payscale` WHERE blog_id = '" . $blogid . "'; \n";
	$wpdb->query($req);
	
	foreach ($payscales as $payscale) {
		$req = "INSERT INTO `".$wpdb->base_prefix."contriboard_payscale` VALUES('".get_bloginfo('name')."','".$blogid."',".$payscale["scale"].",".$payscale["amount"]."); \n";
		$wpdb->query($req);
	}
}

function cb_get_archive() {
	$cbdb = new Contriboard_DB();
	$archives = $cbdb->getArchive($_POST['cycle'], $_POST['month'], $_POST['year'], $_POST['bid']);
	$json = array();

	foreach ($archives as $arc) {
		if (is_multisite()) { 
			switch_to_blog($_POST['bid']);
		}
        
        $usrdata = get_userdata(get_current_user_id());
	    $ctrname = $usrdata->first_name . ' ' . $usrdata->last_name;
	
		$json[] = array(
			"username"		=> $ctrname,
			"userid"		=> $arc->user_id,
			"articleid"     => $arc->article_id,
			"articlename"   => get_the_title($arc->article_id),
			"sharetotal"	=> $arc->sharetotal,
			"moneyearned"	=> $arc->moneyearned,
			"archiveddate"  => $arc->archived_date
		);
	}

	echo json_encode($json); 
	exit();
}

// params : uid
function cb_clear_balance() {
	$userid = $_POST["uid"];
	$blogid = $_POST["bid"];
	$bank = $_POST["bank"];
	$cbdb = new Contriboard_DB();

	echo $cbdb->clearBalance($userid, $bank, $blogid);
	exit();
}
add_action('wp_ajax_cb_clear_balance', 'cb_clear_balance');

// params : uid, amt
function cb_pay_balance() {
	ini_set('display_errors', true);

	$userid = $_POST["uid"];
	$amount = $_POST["amt"];
	$blogid = $_POST["bid"];
	$bank = $_POST["bank"];
	$cbdb = new Contriboard_DB();

	echo $cbdb->payBalance($userid, $amount, $bank, $blogid);
	exit();
}
add_action('wp_ajax_cb_pay_balance', 'cb_pay_balance');

// params : uid, amt
function cb_adjust_balance() {
	$userid = $_POST["uid"];
	$amount = $_POST["amt"];
	$blogid = $_POST["bid"];
	$cbdb = new Contriboard_DB();

	echo $cbdb->adjustBalance($userid, $amount, $blogid);
	exit();
}
add_action('wp_ajax_cb_adjust_balance', 'cb_adjust_balance');

?>
